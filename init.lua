-- Packer
require('plugins')

-- Settings
require('settings')
require('settings.color')
require('settings.keymap')

-- Plugins
require('plugins.compe')
require('plugins.telescope')
require('plugins.gitsigns')
require('plugins.nvim-comment')
require('plugins.nvim-autopairs')
require('plugins.nvimtree')

-- Lsp
require('lsp')

-- Which Key
vim.cmd('source ~/.config/nvim/vimscript/whichkey/init.vim')

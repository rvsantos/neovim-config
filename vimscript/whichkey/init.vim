" Leader Key Maps

" Timeout
let g:which_key_timeout = 100

let g:which_key_display_names = {'<CR>': '↵', '<TAB>': '⇆', " ": 'SPC'}

" Map leader to which_key
nnoremap <silent> <leader> :silent <c-u> :silent WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual '<Space>'<CR>

let g:which_key_map =  {}
let g:which_key_sep = '→'

" Not a fan of floating windows for this
let g:which_key_use_floating_win = 0
let g:which_key_max_size = 0

" Hide status line
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=3 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=3 noshowmode ruler

let g:which_key_map['/'] = [ ':CommentToggle'                 , 'comment toggle']
let g:which_key_map[';'] = [ ':Dashboard'                     , 'home screen' ]
let g:which_key_map['?'] = [ ':NvimTreeFindFile'              , 'find current file' ]
let g:which_key_map['e'] = [ ':NvimTreeToggle'                , 'explorer' ]
let g:which_key_map['f'] = [ ':Telescope find_files'          , 'find files' ]
let g:which_key_map['H'] = [ ':let @/ = ""'                   , 'no highlight' ]

" TODO create entire treesitter section

" Group mappings

" b is for buffer
let g:which_key_map.b = {
      \ 'name' : '+buffer' ,
      \ '>' : [':BufferMoveNext'        , 'move next'],
      \ '<' : [':BufferMovePrevious'    , 'move prev'],
      \ 'b' : [':Telescope buffers'     , 'buffers'],
      \ 'd' : [':BufferClose'           , 'delete-buffer'],
      \ 'n' : ['bnext'                  , 'next-buffer'],
      \ 'p' : ['bprevious'              , 'previous-buffer'],
      \ '?' : ['Buffers'                , 'fzf-buffer'],
      \ }

" F is for fold
let g:which_key_map.F = {
    \ 'name': '+fold',
    \ 'O' : [':set foldlevel=20'  , 'open all'],
    \ 'C' : [':set foldlevel=0'   , 'close all'],
    \ 'c' : [':foldclose'         , 'close'],
    \ 'o' : [':foldopen'          , 'open'],
    \ '1' : [':set foldlevel=1'   , 'level1'],
    \ '2' : [':set foldlevel=2'   , 'level2'],
    \ '3' : [':set foldlevel=3'   , 'level3'],
    \ '4' : [':set foldlevel=4'   , 'level4'],
    \ '5' : [':set foldlevel=5'   , 'level5'],
    \ '6' : [':set foldlevel=6'   , 'level6']
    \ }

" s is for search powered by telescope
let g:which_key_map.s = {
      \ 'name' : '+search' ,
      \ '.' : [':Telescope filetypes'                   , 'filetypes'],
      \ 'B' : [':Telescope git_branches'                , 'git branches'],
      \ 'd' : [':Telescope lsp_document_diagnostics'    , 'document_diagnostics'],
      \ 'D' : [':Telescope lsp_workspace_diagnostics'   , 'workspace_diagnostics'],
      \ 'f' : [":lua require('plugins.telescope').project_files{}"                  , 'files'],
      \ 'h' : [':Telescope command_history'             , 'history'],
      \ 'i' : [':Telescope media_files'                 , 'media files'],
      \ 'm' : [':Telescope marks'                       , 'marks'],
      \ 'M' : [':Telescope man_pages'                   , 'man_pages'],
      \ 'o' : [':Telescope vim_options'                 , 'vim_options'],
      \ 't' : [':Telescope live_grep'                   , 'text'],
      \ 'r' : [':Telescope registers'                   , 'registers'],
      \ 'w' : [':Telescope file_browser'                , 'buf_fuz_find'],
      \ 'u' : [':Telescope colorscheme'                 , 'colorschemes'],
      \ 'p' : [":lua require('telescope').extensions.project.project{}"                 , 'project'],
      \ }

" g is for git
let g:which_key_map.g = {
     \ 'name' : '+git',
     \ 'g' : [':LazyGit'                                                        , 'lazygit'],
     \ 'c' : [":lua require('plugins.telescope').my_git_commits{}"              , 'git commits'],
     \ 's' : [":lua require('plugins.telescope').my_git_status{}"               , 'git status'],
     \ 'b' : [":lua require('plugins.telescope').my_git_bcommits{}"             , 'git bcommits']
     \}

" w is for window
let g:which_key_map.w = {
     \ 'name' : '+window',
     \ 'w' : [':write'                                   , 'save file'],
     \ 'q' : [':q'                                       , 'quit'],
     \ 'v' : ['<C-W>v'                                   , 'split right'],
     \ 's' : ['<C-W>s'                                   , 'split below'],
     \ 'l' : ['<C-W>l'                                   , 'move left window'],
     \ 'h' : ['<C-W>h'                                   , 'move right window'],
     \ 'j' : ['<C-W>j'                                   , 'move bottom window'],
     \ 'k' : ['<C-W>k'                                   , 'move top window'],
     \}

call which_key#register('<Space>', "g:which_key_map")

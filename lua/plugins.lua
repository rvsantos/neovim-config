-- vim.cmd [[packadd packer.nvim]]
local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
  execute 'packadd packer.nvim'
end

--- Check if a file or directory exists in this path
local function require_plugin(plugin)
    local plugin_prefix = fn.stdpath("data") .. "/site/pack/packer/opt/"

    local plugin_path = plugin_prefix .. plugin .. "/"
    --	print('test '..plugin_path)
    local ok, err, code = os.rename(plugin_path, plugin_path)
    if not ok then
        if code == 13 then
            -- Permission denied, but it exists
            return true
        end
    end
    --	print(ok, err, code)
    if ok then
        vim.cmd("packadd " .. plugin)
    end
    return ok, err, code
end

vim.cmd 'autocmd BufWritePost plugins.lua PackerCompile' -- Auto compile when there are changes in plugins.lua

-- require('packer').init({display = {non_interactive = true}})
require('packer').init({display = {auto_clean = false}})

return require('packer').startup(function(use)
  -- Packer can manage itself as an optional plugin
  use 'wbthomason/packer.nvim'

  -- LSP
  use 'neovim/nvim-lspconfig'
  use 'onsails/lspkind-nvim'

  -- Autocomplete
  use {"windwp/nvim-autopairs", opt = true}
  use {"hrsh7th/nvim-compe", opt = true}
  use {"hrsh7th/vim-vsnip", opt = true}
  use {"rafamadriz/friendly-snippets", opt = true}
  
  -- Treesitter
  -- Syntax
  -- Icons
  use 'kyazdani42/nvim-web-devicons'
  use 'ryanoasis/vim-devicons'
  
  -- Status Line and Bufferline
  -- use 'famiu/feline.nvim'
  use 'romgrk/barbar.nvim'

  -- Telescope
  use 'nvim-lua/popup.nvim'
  use 'nvim-lua/plenary.nvim'
  use 'nvim-telescope/telescope.nvim'
  use 'nvim-telescope/telescope-fzy-native.nvim'
  use 'nvim-telescope/telescope-project.nvim'
  use 'fhill2/telescope-ultisnips.nvim'

  -- Explorer
  use 'kyazdani42/nvim-tree.lua'  

  -- Color
  
  -- Git
  use { 'lewis6991/gitsigns.nvim', requires = {'nvim-lua/plenary.nvim'} }
  use 'kdheepak/lazygit.nvim' 

  -- Flutter
  -- Registers
  -- Search and replace
  
  -- Tim Pope docet
  
  -- Tmux
  
  -- WhichKey
  use {"liuchengxu/vim-which-key", opt = true}

  -- Colorschema
  use 'tek256/simple-dark'

  -- General Plugins
  use {"terrortylor/nvim-comment", opt = true}

  require_plugin("vim-which-key")
  require_plugin("nvim-autopairs")
  require_plugin("nvim-comment")
  require_plugin("nvim-compe")
  require_plugin("vim-vsnip")
  require_plugin("friendly-snippets")
  require_plugin("nvim-lspconfig")
end)

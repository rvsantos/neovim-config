vim.api.nvim_set_keymap('n', '<Space>', '<NOP>', {noremap = true, silent = true})
vim.g.mapleader = ' '

-- resize with arrows
vim.cmd([[
  nnoremap <silent> <Down>    :resize -2<CR>
  nnoremap <silent> <Up>  :resize +2<CR>
  nnoremap <silent> <Left>  :vertical resize -2<CR>
  nnoremap <silent> <Right> :vertical resize +2<CR>
]])

vim.api.nvim_set_keymap('n', ';', ':', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'sh', '<C-W>h', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'sj', '<C-W>j', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'sk', '<C-W>k', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'sl', '<C-W>l', {noremap = true, silent = true})

-- better indenting
vim.api.nvim_set_keymap('v', '<', '<gv', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '>', '>gv', {noremap = true, silent = true})

-- I hate escape
vim.api.nvim_set_keymap('i', 'jk', '<ESC>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('i', 'kj', '<ESC>', {noremap = true, silent = true})
